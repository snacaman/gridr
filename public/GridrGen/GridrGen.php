<?php
class GridrGen {

	/**
	 * @var string
	 */
	protected $css = null;

	protected $cssAddCols = false;
	protected $cssCols;

	protected $gutterFixed = false;
	protected $gutterS;

	public function generate($cols, $gutter) {

		$this->cssCols = ($this->cssAddCols) ? $cols : '';
		$this->gutterS = ($this->gutterFixed) ? 'px' : '%';

		$this->genGrid($cols, $gutter);

		$template = file_get_contents( dirname(__FILE__) . '/templates/gridr.in.css');

		//$template = preg_replace('/\/\*.+\*\//', ' ', $template);
		//$template = preg_replace('/\s+/', ' ', $template);
		$template = str_replace('@cols@', $this->cssCols, $template);
		$template = str_replace('@gutter@', $gutter . $this->gutterS, $template);
		$template = str_replace('@spans@', $this->css, $template);

		echo $template;
	}

	protected function genGrid($cols, $gutter) {

		$gutter = $this->gutterFixed ? 0 : $gutter;

		$one = (100 - ($gutter * ($cols - 1))) / $cols;

		for($x=1; $x<=$cols; $x++) {

			$width = ($one * $x) + ($gutter * ($x - 1));
			$off = $width + $gutter;

			$this->css .= ".grid{$this->cssCols} .gs{$x} { width: {$width}%; } ";
			$this->css .= ".grid{$this->cssCols} .gr .push{$x} { left: {$off}%; } ";
			$this->css .= ".grid{$this->cssCols} .gr .pull{$x} { right: {$off}%; } ";
			$this->css .= ".grid{$this->cssCols} .gr .off{$x} { margin-left: {$off}%; } ";
		}

		$this->genNested($cols, $gutter, $one);
	}

	protected function genNested($cols, $gutter, $one) {

		$gutter = $this->gutterFixed ? 0 : $gutter;

		for($x=1; $x<=$cols; $x++) {

			$parent = ($one * $x) + ($gutter * ($x - 1));
			$margin = ($gutter / $parent) * 100;

			for($y=1; $y<=$x; $y++) {

				$child = ($one * $y) + ($gutter * ($y - 1));
				$width = ($child / $parent) * 100;
				$off = $width + $margin;

				$this->css .= ".grid{$this->cssCols} .gs{$x} > .gr > .gs{$y} {width: {$width}%; margin-right: {$margin}%; } ";
				$this->css .= ".grid{$this->cssCols} .gs{$x} > .gr > .off{$y} { margin-left: {$off}%; } ";
				$this->css .= ".grid{$this->cssCols} .gs{$x} > .gr > .push{$y} { left: {$off}%; } ";
				$this->css .= ".grid{$this->cssCols} .gs{$x} > .gr > .pull{$y} { right: {$off}%; } ";
			}
		}
	}
}